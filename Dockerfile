# Build the go binary
FROM golang:1.14 AS builder
WORKDIR $GOPATH/src/gitlab.cern.ch/ci-tools/docker-image-builder
COPY . ./
RUN echo 'Building static go binary ...'
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -mod vendor -o /main .

# Build the final image
FROM gcr.io/kaniko-project/executor:v1.3.0-debug

# Add the previously built app binary to the image
COPY --from=builder /main  /

ENTRYPOINT ["/main"]
